# Intro

*PopTracer* is an R package designed to simulate a population of cells that evolve and compete over a predefined number of cycles. During the simulation, new somatic mutations are progressively introduced, thereby modifying the fitness of carrier cells, and therefore, their survival compared to other cells.  At the end of the process, a population of tumour cells is obtained, with a distribution of somatic mutations derived from the structure of the simulated lineage and the configuration of emerged dominant clones. Interestingly, *PopTracer* can also work in *backward* mode, tracing a candidate tumour lineage that resembles the number and proportion of dominant clones specified by the user.

<br />

# Hands-on tutorial

*PopTracer* can be installed from this gitlab repository by using devtools package:

```{r}
 devtools::install_gitlab("Carbonell/poptracer")
```

Then, we just need to import the package to start working with it


```{r}
library(PopTracer)
```

To perform a simulation with *PopTracer*, we need to start by defining some basic input parameters. In this case, we define the reference genome to be used (in fasta format), and the name of the output folder, which will be used later to write the main simulation results to disk.

```{r}
# Reference genome (Fasta format)
genome <- "../../../resources/Homo_sapiens.GRCh38.dna.chromosome.21.fa"

# Output folder
outdir <- "results"
```

Then, we have to initialise the list of parameters that determine the main characteristics of the simulation to be performed. To do this, we will use the following function:

```{r}
params <- get_default_params()
params
```

At this point, we can modify some of the default values in order to adapt the simulation to the specific requirements of our use case.

```{r}
params$n <- 50
params$pop_size <- 20
params$mu <- .001
params$tsmu <- .5
```

Ahora podemos realizar la simulación, tomando como entrada la lista de parámetros y el genoma de referencia definido anteriormente.

```{r}
sim <- simulate_tumor(params, genome)
```


## Basic plots

After running the simulation, we can make some plots in order to determine how the main characteristics of the cell populations we have simulated have been evolving during the simulation.

First, we can visualise the allele frequency distribution of the somatic mutations that the cells have acquired.

```{r fig.width=5.5, fig.height=4}
plot_somatic_allele_frequencies(sim)
```

![](examples/allele_frequency.png)

An interesting feature is to determine the age of each of the somatic mutations acquired by the population. To do this, in the following plot it is possible to visualise the number of mutations that have appeared in each iteration, marking in red those that have survived in the final population of cells.

```{r fig.width=5.5, fig.height=4}
plot_mutations_at_time(sim)
```

![](examples/mutations_at.png)

On the other hand, during the process, cells will compete with other cells in the tissue on the basis of the effect produced by the somatic mutations present in the carrier cells. This effect can be assessed from the distribution of the fitness value assigned to each somatic mutation, and its value at the cellular level. The following graphs describe both characteristics.

```{r fig.width=5.5, fig.height=4}
plot_somatic_fitness(sim)
```

![](examples/mutation_fitness.png)

```{r fig.width=5.5, fig.height=4}
plot_cell_fitness(sim)
```

![](examples/cell_fitness.png)

Finally, we can visualise the lineage tree corresponding to the population of cells simulated during the process.

```{r fig.width=5.5, fig.height=8}
plot_lineage(sim)
```

![](examples/cell_lineage.png)

## Inferring dominant clone history

The somatic mutations acquired during a simulation determine the fitness of each of the carrier cells, resulting in the emergence of different dominant clones during the processing time, which expand in the tissue until they are displaced by later clones. In this context, *PopTracer* provides the following function to reconstruct the history of the dominant clones by analysing the lineage obtained and their fitness distribution.

```{r}
dch <- infer_dominant_clone_history(sim, fitness = .2, s=20, exp_improvement = 1.045)
```

After the analysis, the dominant clone history can be visualized by using the following function:

```{r}
plot_dominant_clone_history(dch, smooth=T)
```

![](examples/clone_history.png)


The same information can be also displayed but describing the number of cells belonging to each dominant clone across time.

```{r}
plot_dominant_clone_expansion(dch)
```

![](examples/clone_expansion.png)

Finally, the lineage of dominant clones, and their corresponding descendants can be showed as a rooted tree.

```{r}
plot_dominant_clone_lineage(dch)
```

![](examples/clone_lineage.png)


## Backward mode

*PopTracer* can also work in reverse mode, allowing users to specify the dominant clone structure at the end of the simulation. To do this, the user must define the expected proportion of dominant clones in the final population of cells. *PopTraces* takes this information the trace a posible history of dominant clones compatible with this information.


```{r}
params$clone_info <- c(.2, .3, .5)

sim_b <- simulate_tumor(params=params, genome=genome, backward = T, verbose=T)
```

Then, the user can use the same plotting functions to visualize the obtained history of dominant clones.

```{r}
plot_dominant_clone_history(sim_b$pop$clone_structure)
plot_dominant_clone_expansion(sim_b$pop$clone_structure)
plot_dominant_clone_lineage(sim_b$pop$clone_structure)
```

![](examples/clone_history_backward.png)
![](examples/clone_expansion_backward.png)
![](examples/clone_lineage_backward.png)

## Simulating sequencing samples

*PopTracer* uses the individual cell haplotypes generated during the process to create a normal/tumour pair of sequencing samples with a specific depth of coverage defined by the user. In particular, individual cell haplotypes are used to create partial sequencing samples with the corresponding fraction of coverage, that are subsequently combined to create the bulk samples. 

First, the tool has to generate the haplotypes corresponding to each of the cells obtained at the end of the simulation. To do this, *PopTracer* takes the reference genome and inserts the corresponding somatic mutations present in each cell. The haplotypes are then saved to disk in fasta format for later use.

```{r}
all_hh <- get_pop_haplotypes(sim$ref, sim$pop)

save_fasta_pop_haplotypes(all_hh, outdir)
```

Then, *PopTracer* generates the corresponding normal/tumor pair of sequencing samples, by using [ART](https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm) tool in order to add realistic noise artifacts to generated reads. To this end, the user must define the desired coverage, the normal contamination level, and the path to the ART binary.


```{r}
simulate_sample_reads(
  sim$pop, 
  fasta_folder = outdir, 
  cov = 50, 
  normal_cont=.25,
  routdir = outdir,
  noise_shift=0, 
  nbiopsies = 2,
  art_bin = "~/software/art_bin_MountRainier/art_illumina"
)
```

It is important to mention that the user can set the parameter *nbiopsies* to split the tumor samples in several files corresponding to different subgroups of cells. This feature mimics the availability of different biopsies for the same individual, very useful to evaluate tools that reconstruct the phylogeny of clones in a real tumour.



